#include <stdlib.h>
#include <stdio.h>
#include <string.h>


int main(int argc, char *argv[]){

  /**
   * Testing arguments
   */
  if (argc != 5)
  {
    printf("Usage: ex02.c string1 file1 string2 file2.\n");
    exit(0);
  }

  FILE* f1 = fopen(argv[2], "r");
  FILE* f2 = fopen(argv[4], "r+");

  if (f1 == NULL || f2 == NULL)
  {
    printf("Failed opening files.\n");
  }

  char* string1 = argv[1];
  char* string2 = argv[3];
  size_t lenS1 = strlen(string1);
  size_t lenS2 = strlen(string2);

  char* pos; //a pointer, because strstr return a pointer
  char* p;
  int i;
  char line[100];

  while(fgets(line, 100, f1) != NULL)
  {
    i = 0;
    p = &line[i];

    while(line[i] != '\0')
    { 
      pos = strstr(line, string1);
      if (pos == NULL){
        fprintf(f2, line);
        break;
      }
      /* writing the string before the str1 */
      else{
        for(p; p<pos; p++){
          fputc(*p, f2);
          i++;
        }
        /* writing the string1 */
        fprintf(f2, string2);
        i += lenS1;
        /*writing the rest of the string*/
        for(i; line[i] != '\0'; i++){
          fputc(line[i], f2);
        }
      }
    }
  }
  
  fclose(f1);
  fclose(f2);

}
