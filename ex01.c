#include <stdio.h>
#include <stdlib.h>

#define MAX_LENGTH 100

int
main (int argc, char *argv[])
{
  int i;
  char line[100];
  char prevLine[100] = { 0 };
  int countHor = 0;
  int verti = 0;
  int hori = 0;

  FILE *fp = fopen ("input1", "r");
  //if (argc != 2) {
  //  printf ("missing input file or too much arguments");
  //  exit (0);
  //}

  while (fgets (line, 100, fp) != NULL) {
    for (i = 0; line[i] != '\0' && i < 100; i++) {
      //count horizontal sequences
      if (line[i] == 'h') {
	countHor++;
      }
      else
	countHor = 0;
      if (countHor == 5) {
	hori++;
      }
      //try to catch the vertical ones
      if (line[i] == 'v') {
	prevLine[i]++;
      }
      else
	prevLine[i] = 0;

      if (prevLine[i] == 5)
	verti++;
    }
    for (i; i < 100; i++) {
      prevLine[i] = 0;
    }
  }

  fclose (fp);

  printf ("Number of horizontal hhhhh: %d\n", hori);
  printf ("Number of vertical   vvvvv: %d\n", verti);

}
